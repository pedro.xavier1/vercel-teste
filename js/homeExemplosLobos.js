const sectionLobos = document.querySelector('.examples')
var listaLobos;

const pegarListaLobos = () => {
  fetch(`https://lobinhos.herokuapp.com/wolves`)
    .then(res => res.json())
    .then(wolves => {
      listaLobos = wolves.map(wolf => wolf.id)
      const a = listaLobos[(Math.random() * listaLobos.length).toFixed(0)]
      const b = listaLobos[((Math.random() * listaLobos.length).toFixed(0))]

      getRandomWolf(a)
      getRandomWolf(b)
    })
    .catch(() => alert("Ocorreu um erro"))
}

const createWolf = (wolf) => {
  const div = document.createElement('div')
  div.className = 'lobo'
  div.innerHTML = `<img src="${wolf.image_url}" alt="imagem de lobo">
  <div class="detalhes">
    <h2>${wolf.name}</h2>
    <h3>Idade: ${wolf.age} anos</h3>
    <p>${wolf.description}</p>
  </div>`

  sectionLobos.appendChild(div)
}

const getRandomWolf = (id) => {
  fetch(`https://lobinhos.herokuapp.com/wolves/${id}`)
    .then(res => res.json())
    .then(wolf => createWolf(wolf))
    .catch(() => alert("Ocorreu um erro"))
}

pegarListaLobos()
